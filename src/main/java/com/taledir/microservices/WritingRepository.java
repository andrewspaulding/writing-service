package com.taledir.microservices;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WritingRepository extends JpaRepository<Writing, Long>{

  @Query("select new java.lang.String(a.username) " +
  "from Writing w " +
  "join w.authors a " +
  "where w.id = :id")
  Set<String> findAuthorsById(Long id);

  @Query("select new " +
  "   com.taledir.microservices.WritingListItemDto(" +
  "       w.id, w.title, w.publishedRoute, w.snippet, w.lastPublishedDate, w.lastModifiedDate" +
  "   ) " +
  "from Author a " +
  "join a.authorWritings w " +
  "where a.username = :username " +
  "and w.publishedId = null")
  Set<WritingListItemDto> findDraftsByUsername(String username);

  @Query("select new " +
  "   com.taledir.microservices.WritingListItemDto(" +
  "       w.id, w.title, w.publishedRoute, w.snippet, w.lastPublishedDate, w.lastModifiedDate" +
  "   ) " +
  "from Author a " +
  "join a.authorWritings w " +
  "where a.username = :username " +
  "and w.publishedId != null")
  Set<WritingListItemDto> findPublishedByUsername(String username);

  @Query("select new java.lang.Long(w.id) " +
  "from Author a " +
  "join a.ownerWritings w " +
  "where a.username = :username " +
  "and w.lastModifiedDate is null and w.title = null")
  Long findOneUnmodifiedByUsername(String username);

  @Query("select new java.lang.Boolean(w.isEditingLocked) " +
  "from Writing w " +
  "where w.id = :id")
  Boolean findIsEditingLockedById(Long id);

}