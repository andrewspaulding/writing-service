package com.taledir.microservices;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class WritingService {

	@Value("${spring.security.user.name}")
  private String authServiceUsername;

  @Value("${spring.security.user.password}")
  private String authServicePassword;

	Logger logger = LoggerFactory.getLogger(WritingService.class);

  private static final Pattern NONLATIN = Pattern.compile("[^\\w_-]");
  private static final Pattern SEPARATORS = Pattern.compile("[\\s\\p{Punct}&&[^-]]");

  private static final Integer MAX_SNIPPET_LENGTH = 200;

	@Autowired
  WritingRepository writingRepository;
  
	@Autowired
	WritingBodyRepository writingBodyRepository;

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

  private Delta convertStringToDelta(String body) {
		ObjectMapper objectMapper = new ObjectMapper();
		Delta delta;
		try {
			delta = objectMapper.readValue(body, Delta.class);
			return delta;
		} catch (JsonMappingException e) {
			throw new WritingBadRequestException(e.getMessage());
		} catch (JsonProcessingException e) {
			throw new WritingBadRequestException(e.getMessage());
		}
  }

  protected WritingDto retrieveWriting(Long id) {
    Optional<Writing> writingOptional = writingRepository.findById(id);

    if(!writingOptional.isPresent()) {
      throw new WritingNotFoundException("Writing does not exist");
    }

    Writing writing = writingOptional.get();
    
    Author owner = writing.getOwner();

    String ownerUsername = owner.getUsername();

    Set<String> authorUsernames = new HashSet<String>();
    
    for (Author author : writing.getAuthors()) {
      authorUsernames.add(author.getUsername());
    }

    Delta delta = convertStringToDelta(writing.getWritingBody().getBody());
    return new WritingDto(writing.getTitle(), delta, ownerUsername, 
      writing.getLastPublishedTitle(), writing.getSubtitle(), writing.getSnippet(), 
      writing.getLastPublishedDate(), writing.getLastModifiedDate(),
      authorUsernames, writing.getPublishedRoute(), writing.getIsEditingLocked());
	}

	protected void updateStoryBody(Long id, Delta newDelta) {

    Writing writing = writingRepository.getOne(id);

		if(newDelta == null || newDelta.getOps() == null) {
			throw new WritingBadRequestException("Bad request provided for saving story.");
		}

		if(writing == null) {
			throw new WritingNotFoundException("Cannot save, story does not exist");
		}

    if(writing.getIsEditingLocked()) {
      throw new WritingBadRequestException("Cannot update story, editing is currently locked.");
    }

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String oldDeltaString = writing.getWritingBody().getBody();

			Delta oldDelta = objectMapper.readValue(oldDeltaString, Delta.class);

			DeltaMerge deltaMerge = new DeltaMerge(oldDelta, newDelta);

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			HttpEntity<DeltaMerge> requestEntity = new HttpEntity<DeltaMerge>(deltaMerge, headers);
		
			ResponseEntity<Delta> responseEntity = 
				restTemplate().exchange("http://localhost:3009/delta/merge", 
				HttpMethod.POST, requestEntity, Delta.class);

			Delta mergedDelta = responseEntity.getBody();

			String body = objectMapper.writeValueAsString(mergedDelta);

      writing.getWritingBody().setBody(body);
      writing.setLastModifiedDate(new Date());

			writingRepository.save(writing);
			
		} catch (JsonMappingException e) {
			throw new WritingBadRequestException(e.getMessage());
		} catch (JsonProcessingException e) {
			throw new WritingBadRequestException(e.getMessage());
		}
	}

	protected String getTokenUsername(String token) {
		
		if(token != null && token.length() > 0) {
			Map<String, Object> responseBody = getIntrospectResponse(token);

			if(responseBody != null) {
				Object username = responseBody.get("username");
				if(username != null)  {
					return username.toString();
				}
			}
		}
		return null;
	}

	private Map<String, Object> getIntrospectResponse(String token) {
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		reqHeaders.setBasicAuth(authServiceUsername, authServicePassword);
		
		MultiValueMap<String, String> body= new LinkedMultiValueMap<String, String>();
		body.add("token", token);
		
		HttpEntity<MultiValueMap<String, String>> requestEntity = 
			new HttpEntity<MultiValueMap<String, String>>(body, reqHeaders);

		ResponseEntity<Object> responseEntity;
		try {
			responseEntity = restTemplate().exchange(
				"http://taledir-test.com/auth/introspect", 
			HttpMethod.POST, requestEntity, Object.class);

		} catch(HttpClientErrorException e) {
			logger.error("Authorization server username or password is incorrect.  " + 
				"This will prevent authorization!");
			return null;
		} catch(RestClientException e) {
			logger.error("Authorization server hostname is not setup correctly.  " + 
				"This will prevent authorization!");
			return null;
		}
		Object oBody = responseEntity.getBody();
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, Object> responseBody = 
			oMapper.convertValue(oBody, new TypeReference<Map<String, Object>>() {});
			
		return responseBody;
  }

	protected boolean isValidReadWritePermissions(Long id, String token) {
		
		Set<String> authors = writingRepository.findAuthorsById(id);

		if(authors.size() > 0) {

			Map<String, Object> responseBody = getIntrospectResponse(token);
			if(responseBody == null) {
				return false;
			}
			
			Object responseUsername = responseBody.get("username");

			if(responseUsername != null && authors.contains(responseUsername.toString()))  {
				return true;
			}
		}
		return false;
  }
  
	private boolean isValidOwnerPermissions(String token, Writing writing) {
		
    Author owner = writing.getOwner();
    
    Map<String, Object> responseBody = getIntrospectResponse(token);
    if(responseBody == null) {
      return false;
    }
    
    Object responseUsername = responseBody.get("username");

    if(responseUsername != null && owner.getUsername().equals(responseUsername.toString()))  {
      return true;
    }
		return false;
	}

 
  private String convertDeltaToHtml(String body) {

    Delta delta = convertStringToDelta(body);
    
		HttpHeaders reqHeaders = new HttpHeaders();
		reqHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<Delta> requestEntity = new HttpEntity<Delta>(delta, reqHeaders);
	
		ResponseEntity<String> responseEntity = 
			restTemplate().exchange("http://localhost:3009/delta/tohtml", 
			HttpMethod.POST, requestEntity, String.class);
		String html = responseEntity.getBody();

		return html;
	}
  
  protected Published publish(String token, Writing writing) {

    if(writing.getLastPublishedDate() != null &&
      writing.getLastModifiedDate().before(writing.getLastPublishedDate())) {
      throw new WritingBadRequestException("Nothing to publish, no new changes have been made since last publish.");
    }

    if(writing.getTitle() == null) {
      throw new WritingBadRequestException("Writing must contain title in order to publish");
    }

    String writingBody = writing.getWritingBody().getBody();

    String htmlBody = convertDeltaToHtml(writingBody);

    String textOnly = htmlBody.replaceAll("\\<.*?\\>", "");

    if(textOnly == null || textOnly.length() == 0) {
      throw new WritingBadRequestException("Writing must contain text in order to publish");
    }

    //set a default snippet if one doesn't exist
    if(writing.getSnippet() == null) {
      if(textOnly.length() > MAX_SNIPPET_LENGTH) {
        writing.setSnippet(textOnly.substring(0, MAX_SNIPPET_LENGTH));
      }
      else {
        writing.setSnippet(textOnly);
      }
    }

		HttpHeaders reqHeaders = new HttpHeaders();
    reqHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

    String slug = convertTitleToSlug(writing.getTitle());

    if(slug == null || slug.length() == 0) {
      throw new WritingBadRequestException("Title must contain at least one letter or one number");
    }

    Set<String> authorUsernames = new HashSet<String>();

    for(Author author : writing.getAuthors()) {
      authorUsernames.add(author.getUsername());
    }

    PublishWritingDto body = new PublishWritingDto(
      token, writing.getPublishedId(), writing.getTitle(), htmlBody, 
      writing.getSubtitle(), slug, writing.getSnippet(), authorUsernames
    );

    HttpEntity<PublishWritingDto> requestEntity = 
      new HttpEntity<PublishWritingDto>(body, reqHeaders);
  
    String endpoint;
    if(writing.getPublishedId() == null) {
      endpoint = "create";
    }
    else {
      endpoint = "update";
    }
    ResponseEntity<Published> responseEntity;
    try {
      responseEntity = 
			  restTemplate().exchange("http://taledir-test.com/stories/" + endpoint, 
        HttpMethod.POST, requestEntity, Published.class);
    } catch(HttpClientErrorException e) {
      try {
        ObjectMapper objectMapper = new ObjectMapper();
        String errorBody = e.getResponseBodyAsString();
        RestError restError;
        restError = objectMapper.readValue(errorBody, RestError.class);
        throw new WritingBadRequestException(restError.message);
      } catch (JsonProcessingException e1) {
        logger.error("Story service is throwing error which cannot be parsed!");
        throw new WritingBadRequestException("There was an unexpected error when publishing.");
      }
    } catch(RestClientException e) {
      logger.error("Story server is not setup correctly.  " + 
        "This will prevent publishing!");
      throw new WritingBadRequestException("There was an unexpected error when publishing, please try again later.");
    }

    Published published = responseEntity.getBody();

    Long publishedId = published.getId();

    //last modified date on story service is lastPublishedDate on writing service
    Date lastPublishedDate = published.getLastModifiedDate();
    writing.setLastPublishedDate(lastPublishedDate);
    writing.setLastPublishedTitle(published.getTitle());

    writing.setPublishedRoute(published.getRoute());

    if(writing.getPublishedId() == null) {
      writing.setPublishedId(publishedId);
    }
    else if(writing.getPublishedId() != publishedId) {
      throw new WritingBadRequestException("An unexpected issue has occurred.  Please contact administrator.");
    }

    writingRepository.save(writing);

    return published;
  }

  protected String convertTitleToSlug(String title) {

    String noseparators = SEPARATORS.matcher(title).replaceAll("-");
    String normalized = Normalizer.normalize(noseparators, Form.NFD);
    String slug = NONLATIN.matcher(normalized).replaceAll("");
    String lowercaseSlug = slug.toLowerCase(Locale.ENGLISH).replaceAll("-{2,}","-").replaceAll("^-|-$","");
    
		return lowercaseSlug;
  }

  protected Writing getWritingForOwner(String token, Long id) {
    if(id == null) {
      throw new WritingBadRequestException("Please provide story ID to publish.");
    }

    Writing writing = writingRepository.getOne(id);
    if(writing == null) {
      throw new WritingNotFoundException("Cannot publish story, id does not exist.");
    }
		
		if(token == null || token.length() == 0 ||
		!isValidOwnerPermissions(token, writing)) {
			throw new WritingBadRequestException(
				"Cannot publish story.  " + 
				"Please try to login first, and verify your permissions.");
		}

    return writing;
    
  }

  protected void lockEditing(Writing writing) {
    writing.setIsEditingLocked(true);
    writingRepository.save(writing);
  }

  protected void unlockEditing(Writing writing) {
    writing.setIsEditingLocked(false);
    writingRepository.save(writing);
  }

}
