package com.taledir.microservices;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "author")
public class Author {

	@Id
  @Column(name = "id")
  @JsonIgnore
	private Integer id;

	@Column(name = "username", nullable = false)
	private String username;

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "authors")
	@JsonIgnore
  Set<Writing> authorWritings;
  
  @OneToMany(
    mappedBy = "owner",
    cascade = CascadeType.ALL,
    orphanRemoval = true
  )
  @JsonIgnore
  Set<Writing> ownerWritings;

	protected Author() {

	}

	public Author(Integer id, String name, String username) {
		super();
		this.id = id;
		this.username = username;
	}

	public Integer getId() {
		return id;
	}

  public void setId(Integer id) {
      this.id = id;
  }

	public String getUsername() {
		return username;
	}

  public void setUsername(String username) {
      this.username = username;
  }

	public Set<Writing> getAuthorWritings() {
		return authorWritings;
	}

	public void setAuthorWritings(Set<Writing> authorWritings) {
		this.authorWritings = authorWritings;
	}

	public Set<Writing> getOwnerWritings() {
		return ownerWritings;
	}

	public void setOwnerWritings(Set<Writing> ownerWritings) {
		this.ownerWritings = ownerWritings;
	}

}
