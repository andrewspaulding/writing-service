package com.taledir.microservices;

import java.util.List;

public class Delta {

	private List<Object> ops;

	public Delta() {
		super();
	}

	public Delta(List<Object> ops) {
		this.ops = ops;
	}

	public List<Object> getOps() {
		return ops;
	}

	public void setOps(List<Object> ops) {
		this.ops = ops;
	}

}
