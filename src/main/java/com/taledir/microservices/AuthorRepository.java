package com.taledir.microservices;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long>{

    Author findOneByUsername(String username);

}