package com.taledir.microservices;

public class DeltaMerge {

	private Delta oldDelta;

	private Delta newDelta;

	public DeltaMerge(Delta oldDelta, Delta newDelta) {
		this.oldDelta = oldDelta;
		this.newDelta = newDelta;
	}

	public Delta getOldDelta() {
		return oldDelta;
	}

	public void setOldDelta(Delta oldDelta) {
		this.oldDelta = oldDelta;
	}

	public Delta getNewDelta() {
		return newDelta;
	}

	public void setNewDelta(Delta newDelta) {
		this.newDelta = newDelta;
	}

}
